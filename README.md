![Alt text](readmeResources/readme_svg/made-with-java.svg)

![Alt text](readmeResources/readme_svg/uses-springboot.svg)

![Alt text](readmeResources/readme_svg/uses-vuejs.svg)


# POEC CYBERSECURITE 2022 - EPSI Rennes

## Guitar Shop Java App

### V 1.0.0 : 
    - FrontEnd VueJS show list / filtered list, create order
    - BackEnd Java API, Get & Post products
### Run :

- import guitarshop.sql
```shell
cd src/guitar-shop-vue
npm i
npm run serve
--------------------
start java server
--------------------
http://localhost:1969/
```

### Pour l'evaluation, le Java rendu n'est pas a la hauteur de mes esprances, mais j'ai pris beaucoup de temps pour implementer VueJS dans un projet SpringBoot en prévision de la MSPR (Securite d'un bus d'evenements, des props, VueX, VueRouter)